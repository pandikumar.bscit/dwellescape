var sc3BOX0001arrowObj = { 0: 'arrow0001', 1: 'arrow0002', 2: 'arrow0003', 3: 'arrow0004' };
var sc3BOX0001arrowObjResulT = { 0: 'arrow0004', 1: 'arrow0001', 2: 'arrow0002', 3: 'arrow0003' };

var sc3BOX0001buttonObj = { 0: 'color0001', 1: 'color0001', 2: 'color0001', 3: 'color0001' };
var sc3BOX0001buttonObjResulT = { 0: 'color0004', 1: 'color0003', 2: 'color0002', 3: 'color0001' };

var sc3puzzlebuttonObj = { 0: 'puzz0001', 1: 'puzz0002', 2: 'puzz0003', 3: 'puzz0004', 4: 'puzz0005', 5: 'puzz0006', 6: 'puzz0007', 7: 'puzz0008', 8: 'puzz0009' };
var sc3puzzlebuttonObjResulT = { 0: 'puzz0009', 1: 'puzz0008', 2: 'puzz0007', 3: 'puzz0006', 4: 'puzz0005', 5: 'puzz0004', 6: 'puzz0003', 7: 'puzz0002', 8: 'puzz0001' };

var sc3circle9buttonObj = { 0: 'click_button0001', 1: 'click_button0001', 2: 'click_button0001', 3: 'click_button0001', 4: 'click_button0001', 5: 'click_button0001', 6: 'click_button0001', 7: 'click_button0001', 8: 'click_button0001' };
// var sc3circle9buttonObjResulT = { 0: 'click_button0002', 1: 'click_button0001', 2: 'click_button0001', 3: 'click_button0001', 4: 'click_button0001', 5: 'click_button0001', 6: 'click_button0001', 7: 'click_button0001', 8: 'click_button0001' };
var sc3circle9buttonObjResulT = { 0: 'click_button0002', 1: 'click_button0001', 2: 'click_button0002', 3: 'click_button0001', 4: 'click_button0002', 5: 'click_button0002', 6: 'click_button0002', 7: 'click_button0002', 8: 'click_button0001' };

function defaultDisplaySc3() {
    if(!$(".btn.zoom.object.f3BOX0001arrow").hasClass('final')){
        $(".btn.zoom.object.f3BOX0001arrow").html('');
        $.each(sc3BOX0001arrowObj, function (key, value) {
            $(".btn.zoom.object.f3BOX0001arrow").append("<button class='btn s3 f3BOX0001arrow change object arrow" + key + "' data-cur-val='" + value + "'><img src='img/" + value + ".png' /></div>");
        });
    }
    if(!$(".btn.zoom.object.f3BOX0001button").hasClass('final')){
        $(".btn.zoom.object.f3BOX0001button").html('');
        $.each(sc3BOX0001buttonObj, function (key, value) {
            $(".btn.zoom.object.f3BOX0001button").append("<button class='btn s3 f3BOX0001button change object arrow" + key + "' data-cur-val='" + value + "'><img src='img/" + value + ".png' /></div>");
        });
    }
    if(!$(".btn.zoom.object.f3BOX0001puzzle").hasClass('final')){
        $(".btn.zoom.object.f3BOX0001puzzle").html('');
        $.each(sc3puzzlebuttonObjResulT, function (key, value) {
            $(".btn.zoom.object.f3BOX0001puzzle").append("<button class='btn s3 f3BOX0001puzzle change object arrow" + key + "' data-cur-val='" + value + "'><img src='img/" + value + ".png' /></div>");
        });
    }
    if(!$(".btn.zoom.object.f3BOX0001circle9").hasClass('final')){
        $(".btn.zoom.object.f3BOX0001circle9").html('');
        $.each(sc3circle9buttonObj, function (key, value) {
            $(".btn.zoom.object.f3BOX0001circle9").append("<button class='btn s3 f3BOX0001circle9 change object arrow" + key + "' data-cur-val='" + value + "'><img src='img/" + value + ".png' /></div>");
        });
    }
}

function zoomobjectsc3(thise, oj) {
    zoom_obj = oj;
    $('.screen-3').hide();
    $('.screen-3-zoom-' + oj).show();
    displaysc3arrowObjboxAob();
    displaysc3buttonObjboxAob();
    displaysc3puzzleButtonboxAob();
    displaysc3circle9ButtonboxAob();
}

function displaysc3arrowObjboxAob() {
    $(".screen-3-zoom-f3BOX0001arrow .screen-object.f3BOX0001arrow").html('');
    $.each(sc3BOX0001arrowObj, function (key, value) {
        $(".screen-3-zoom-f3BOX0001arrow .screen-object.f3BOX0001arrow").append("<button class='btn s3 f3BOX0001arrow change object arrow" + key + "' data-cur-val='" + value + "' onclick=\"changeobjectsc3arrowObjbox(this," + key + ")\"><img src='img/" + value + ".png' /></div>");
    });
}
function changeobjectsc3arrowObjbox(thise, key) {
    var oj = $('.btn.s3.f3BOX0001arrow.change.object.arrow' + key).attr("data-cur-val");

    var lastChar = parseInt(oj.slice(-1));
    if (lastChar == 4) {
        lastChar = 1;
    } else {
        lastChar = lastChar + 1;
    }
    sc3BOX0001arrowObj[key] = 'arrow000' + lastChar;
    $('.btn.s3.f3BOX0001arrow.change.object.arrow' + key).attr("data-cur-val", sc3BOX0001arrowObj[key]);
    $('.btn.s3.f3BOX0001arrow.change.object.arrow' + key).html("<img src='img/" + sc3BOX0001arrowObj[key] + ".png' />");

    var allTrue = JSON.stringify(sc3BOX0001arrowObj)==JSON.stringify(sc3BOX0001arrowObjResulT);
    if (allTrue) {
        $('.screen-3-zoom-f3BOX0001arrow .screen-object.f3BOX0001arrow-box0002').show();
        $('.screen-3-zoom-f3BOX0001arrow .screen-object.f3BOX0001arrow').hide();
        $('.screen-3 .btn.zoom.object.f3BOX0001arrow').addClass('final').html("<img src='img/scroll_scroll1.png' />");
    }
}
function displaysc3buttonObjboxAob() {
    $(".screen-3-zoom-f3BOX0001button .screen-object.f3BOX0001button").html('');
    $.each(sc3BOX0001buttonObj, function (key, value) {
        $(".screen-3-zoom-f3BOX0001button .screen-object.f3BOX0001button").append("<button class='btn s3 f3BOX0001button change object arrow" + key + "' data-cur-val='" + value + "' onclick=\"changeobjectsc3buttonObjbox(this," + key + ")\"><img src='img/" + value + ".png' /></div>");
    });
}
function changeobjectsc3buttonObjbox(thise, key) {
    var oj = $('.btn.s3.f3BOX0001button.change.object.arrow' + key).attr("data-cur-val");

    var lastChar = parseInt(oj.slice(-1));
    if (lastChar == 4) {
        lastChar = 1;
    } else {
        lastChar = lastChar + 1;
    }
    sc3BOX0001buttonObj[key] = 'color000' + lastChar;
    $('.btn.s3.f3BOX0001button.change.object.arrow' + key).attr("data-cur-val", sc3BOX0001buttonObj[key]);
    $('.btn.s3.f3BOX0001button.change.object.arrow' + key).html("<img src='img/" + sc3BOX0001buttonObj[key] + ".png' />");

    var allTrue = JSON.stringify(sc3BOX0001buttonObj)==JSON.stringify(sc3BOX0001buttonObjResulT);
    if (allTrue) {
        invObj.splice(invObj.indexOf('color_clue'), 1);
        displayInvitems();
        $('.screen-3-zoom-f3BOX0001button .screen-object.f3BOX0001button-box0002').show();
        $('.screen-3-zoom-f3BOX0001button .screen-object.f3BOX0001button').hide();
        $('.screen-3 .btn.zoom.object.f3BOX0001button').addClass('final').html("<img class='sc3ref hou-obj1' src='img/hou-obj1.png' /><img  class='sc5ref hou-obj1' src='img/hou-obj1.png' />");
    }
}
function displaysc3puzzleButtonboxAob() {
    $(".screen-3-zoom-f3BOX0001puzzle .screen-object.f3BOX0001puzzle").html('');
    $.each(sc3puzzlebuttonObj, function (key, value) {
        $(".screen-3-zoom-f3BOX0001puzzle .screen-object.f3BOX0001puzzle").append("<button class='btn s3 f3BOX0001puzzle change object arrow" + key + "' data-cur-val='" + value + "' onclick=\"changeobjectsc3puzzleButtonObjbox(this," + key + ")\"><img src='img/" + value + ".png' /></div>");
    });
}
var parentSc3puzzle='';
var parentSc3puzzleIndex='';
function changeobjectsc3puzzleButtonObjbox(thise, key) {
    var oj = $(thise).attr("data-cur-val");
    if(parentSc3puzzle=='') {
        parentSc3puzzle=oj;
        parentSc3puzzleIndex=key;
    } else {
        sc3puzzlebuttonObj[parentSc3puzzleIndex]=oj;
        sc3puzzlebuttonObj[key]=parentSc3puzzle;
        parentSc3puzzle='';
    } 
    displaysc3puzzleButtonboxAob();
    var allTrue = true;
    $.each(sc3puzzlebuttonObj, function (key, value) {
        if (allTrue && sc3puzzlebuttonObj[key] != sc3puzzlebuttonObjResulT[key]) {
            allTrue = false;
        }
    });
    if (allTrue) {
        $('.screen-3-zoom-f3BOX0001puzzle .screen-object.f3BOX0001puzzle-box0002').show();
        $('.screen-3-zoom-f3BOX0001puzzle .screen-object.f3BOX0001puzzle').hide();
        $('.screen-3 .btn.zoom.object.f3BOX0001puzzle').addClass('final').html("<img class='sc3ref hou-obj1' src='img/hou-obj1.png' />");
    }
        
}

function displaysc3circle9ButtonboxAob() {
    $(".screen-3-zoom-f3BOX0001circle9 .screen-object.f3BOX0001circle9").html('');
    $.each(sc3circle9buttonObj, function (key, value) {
        $(".screen-3-zoom-f3BOX0001circle9 .screen-object.f3BOX0001circle9").append("<button class='btn s3 f3BOX0001circle9 change object arrow" + key + "' data-cur-val='" + value + "' onclick=\"changeobjectsc3circle9Objbox(this," + key + ")\"><img src='img/" + value + ".png' /></div>");
    });
}
function changeobjectsc3circle9Objbox(thise, key) {
    var oj = $('.btn.s3.f3BOX0001circle9.change.object.arrow' + key).attr("data-cur-val");
    sc3circle9buttonObj[key] = (sc3circle9buttonObj[key]=='click_button0002')?'click_button0001':'click_button0002';
    $('.btn.s3.f3BOX0001circle9.change.object.arrow' + key).attr("data-cur-val", sc3circle9buttonObj[key]);
    $('.btn.s3.f3BOX0001circle9.change.object.arrow' + key).html("<img src='img/" + sc3circle9buttonObj[key] + ".png' />");

    var allTrue = JSON.stringify(sc3circle9buttonObj)==JSON.stringify(sc3circle9buttonObjResulT);
    if (allTrue) {
        $('.screen-3-zoom-f3BOX0001circle9 .screen-object.f3BOX0001circle9-box0002').show();
        $('.screen-3-zoom-f3BOX0001circle9 .screen-object.f3BOX0001circle9').hide();
        $('.screen-3 .btn.zoom.object.f3BOX0001circle9').addClass('final').html("<img src='img/scroll_scroll3.png' />");
    }
}