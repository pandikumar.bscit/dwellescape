
const canvas = $('#canvas');
var invObj = [];
var zoom_obj = '';
var openupObject = ['scroll_scroll1', 'scroll_scroll2', 'color_clue', 'scroll_scroll3', 'hight_clue', 'scroll_scroll4'];

function start() {
    $('#initial').hide();
    $('#screens').show();
    $('.screen-1').show();
    displayInvitems();
    $("#myAudiobg")[0].play();
    defaultDisplaySc1();
}

function audio() {
    if ($(".btn.audio").hasClass("yes")) {
        $("#myAudiobg")[0].pause();
        $(".btn.audio").removeClass("yes").addClass('mute');
        $(".btn.audio img").attr("src", "img/BUTTONS_0003.png");
    } else {
        $("#myAudiobg")[0].play();
        $(".btn.audio").removeClass("mute").addClass('yes');
        $(".btn.audio img").attr("src", "img/BUTTONS_0002.png");
    }
}

function invscrollup() {
    $('.inventory-items').scrollLeft($('.inventory-items').scrollLeft() - 100);
}

function invscrolldown() {
    $('.inventory-items').scrollLeft($('.inventory-items').scrollLeft() + 100);
}
function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev, obj1inv) {
    ev.dataTransfer.setData("text", ev.target.id);
    ev.dataTransfer.setData("obj1inv", obj1inv);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    var obj1inv = ev.dataTransfer.getData("obj1inv");
    invObj.splice(invObj.indexOf(obj1inv), 1);
    sc2s4Box2Obj.push(obj1inv);
    ev.target.appendChild(document.getElementById(data));
    displayInvitems();
}

function collectobject(thise, oj) {
    $("#myAudiocollect")[0].play();
    thise.remove();
    invObj.push(oj);
    displayInvitems();
    console.log(zoom_obj);
    if (zoom_obj != '') {
        if (zoom_obj == 'heightdot') {
            if ($('.screen-object.heightdot-box0002 .btn.collect.object.hand').length > 0) {
                zoom_obj = 'scroll_scroll4';
                $('.sc5ref.heightdot').remove();
            }
            if ($('.screen-object.heightdot-box0002 .btn.collect.object.scroll_scroll4').length > 0) {
                zoom_obj = 'heightdot';
                $('.sc5ref.hand_0002').remove();
            } 
            if ($('.screen-object.heightdot-box0002 .btn.collect.object').length==0){
                $('.btn.zoom.object.' + zoom_obj + '.final').html('').removeAttr('onclick');
                zoom_obj = '';
            }
        } else if (zoom_obj == 'f3BOX0001button') {
            if ($('.screen-object.f3BOX0001button-box0002 .btn.collect.object.hou-obj1').length > 0) {
                zoom_obj = 'f3BOX0001button';
                $('.btn.zoom.object.' + zoom_obj + '.final .sc3ref.hou-obj1')[0].remove();
            } else {
                $('.btn.zoom.object.' + zoom_obj + '.final').html('').removeAttr('onclick');
                zoom_obj = '';
            }
        } else if (zoom_obj == 'lrbox') {
            if ($('.screen-object.lrbox-box0002 .btn.collect.object.hand').length > 0) {
                zoom_obj = 'lrbox';
                $('.sc1ref.hand')[0].remove();
            } else {
                $('.btn.zoom.object.' + zoom_obj + '.final').html('').removeAttr('onclick');
                zoom_obj = '';
            }
        } else if (zoom_obj == 'box_remote') {
            if ($('.screen-object.box_remote-box0002 .btn.collect.object.ball-obj').length > 0) {
                zoom_obj = 'box_remote';
                $('.sc1ref.ball-obj')[0].remove();
            } else {
                $('.btn.zoom.object.' + zoom_obj + '.final').html('').removeAttr('onclick');
                zoom_obj = '';
            }
        } else if (zoom_obj == 'objectdiamond9') {
            if ($('.screen-object.objectdiamond9-box0002 .btn.collect.object.hight_clue').length > 0) {
                zoom_obj = 'hight_clue';
                $('.sc1ref.hand_0002').remove();
            } else if ($('.screen-object.objectdiamond9-box0002 .btn.collect.object.hand').length > 0) {
                zoom_obj = 'hand_0002';
                $('.sc1ref.hight_clue').remove();
            } else {
                $('.btn.zoom.object.' + zoom_obj + '.final').html('').removeAttr('onclick');
                zoom_obj = '';
            }
        } else {
            $('.btn.zoom.object.' + zoom_obj + '.final').html('').removeAttr('onclick');
            zoom_obj = '';
        }

    }
}

function displayInvitems() {
    $(".inventory-items").html('');
    $.each(invObj, function (key, value) {
        if (openupObject.includes(value)) {
            $(".inventory-items").append("<div class='inv-item draggable click item" + value + "'><img  data-cur-val='" + value + "' onclick=\"opensctip(this,'" + value + "')\" id='inv-item-id" + key + "' src='img/" + value + ".png' /></div>");
        } else {
            $(".inventory-items").append("<div class='inv-item draggable item" + value + "'><img  id='inv-item-id" + key + "' draggable='true' ondragstart=\"drag(event,'" + value + "')\" src='img/" + value + ".png' /></div>");
        }
    });
    if (invObj.length < 5) {
        for (var ioj = 0; ioj < (5 - invObj.length); ioj++) {
            $(".inventory-items").append("<div class='inv-item item" + ioj + "'></div>");
        }
    }
}


function gotosc1home() {
    $('.screen-1').show();
    $('.screen-2').hide();
    $('.screen-3').hide();
    $('.screen-4').hide();
    $('.screen-5').hide();
    $('.screen-1-zoom-box_remote').hide();
    $('.screen-1-zoom-horsecollector').hide();
    $('.screen-1-zoom-objectdiamond9').hide();
    $('.screen-1-zoom-lrbox').hide();
    defaultDisplaySc1();
}
function gotosc2home() {
    $('.screen-1').hide();
    $('.screen-2').show();
    $('.screen-3').hide();
    $('.screen-4').hide();
    $('.screen-5').hide();
    $('.screen-2-zoom-box_color_no').hide();
    $('.screen-2-zoom-bot').hide();
    $('.screen-2-zoom-f2BOX0001dot').hide();
    $('.screen-2-zoom-handdrop').hide();
    defaultDisplaySc2();
}
function gotosc3home() {
    $('.screen-1').hide();
    $('.screen-2').hide();
    $('.screen-3').show();
    $('.screen-4').hide();
    $('.screen-5').hide();
    $('.screen-3-zoom-f3BOX0001arrow').hide();
    $('.screen-3-zoom-f3BOX0001button').hide();
    $('.screen-3-zoom-f3BOX0001puzzle').hide();
    $('.screen-3-zoom-f3BOX0001circle9').hide();
    defaultDisplaySc3();
}
function gotosc4home() {
    $('.screen-1').hide();
    $('.screen-2').hide();
    $('.screen-3').hide();
    $('.screen-4').show();
    $('.screen-5').hide();
    $('.screen-4-zoom-wallcirclebox').hide();
    defaultDisplaySc4();
}
function gotosc5home() {
    $('.screen-1').hide();
    $('.screen-2').hide();
    $('.screen-3').hide();
    $('.screen-4').hide();
    $('.screen-5').show();
    $('.screen-5-zoom-symobj').hide();
    $('.screen-5-zoom-heightdot').hide();
    defaultDisplaySc5();
}


function opensctip(thise, key) {
    $('.screen-opensctip-paper0').hide();
    // $('.screen-opensctip-paper2').hide();
    $('.screen-opensctip-' + key).show();

}
function closesctip(key) {
    $('.screen-opensctip-' + key).hide();
}

$(function () {
    $("#includedContent").load("b.html");
});
$(function () {
    var includes = $('[data-include]')
    $.each(includes, function () {
        var file = $(this).data('include') + '.html';
        $(this).load(file);
    })
})

