var sc2ColorNoDigitboxObj = { 0: 'number/1', 1: 'number/1', 2: 'number/1', 3: 'number/1' };
// var sc2ColorNoDigitboxObjResult = { 0: 'number/2', 1: 'number/1', 2: 'number/1', 3: 'number/1' };
var sc2ColorNoDigitboxObjResult = { 0: 'number/6', 1: 'number/3', 2: 'number/2', 3: 'number/4' };

var sc2DotboxObj = { 0: 'LR_button', 1: 'LR_button'};
var sc2DotboxObjPushResult = [];
var sc2DotboxObjResult = [0,1,0,1,0,0,1];

var sc2DotboxObj = { 0: 'LR_button', 1: 'LR_button'};

var sc2handdropboxO = [];
var sc2handdropboxObj = { 0: 'hand_0001', 1: 'hand_0001', 2: 'hand_0001', 3: 'hand_0001' };
// var sc2handdropboxResulT = { 0: 'hand_0001', 1: 'hand_0001', 2: 'hand_0001', 3: 'hand_0001' };
var sc2handdropboxResulT = { 0: 'hand_0002', 1: 'hand_0002', 2: 'hand_0002', 3: 'hand_0002' };


function defaultDisplaySc2() {
    if(!$(".btn.zoom.object.box_color_no").hasClass('final')){
        $(".btn.zoom.object.box_color_no").html('');
        $.each(sc2ColorNoDigitboxObj, function (key, value) {
            $(".btn.zoom.object.box_color_no").append("<button class='btn s2 box_color_no change object arrow" + key + "' data-cur-val='" + value + "'><img src='img/" + value + ".png' /></div>");
        });
    }
    if(!$(".btn.zoom.object.f2BOX0001dot").hasClass('final')){
        $(".btn.zoom.object.f2BOX0001dot").html('');
        $.each(sc2DotboxObj, function (key, value) {
            $(".btn.zoom.object.f2BOX0001dot").append("<button class='btn s2 f2BOX0001dot change object arrow" + key + "' data-cur-val='" + value + "'><img src='img/" + value + ".png' /></div>");
        });
    }
    if(!$(".btn.zoom.object.handdrop").hasClass('final')){
        $(".btn.zoom.object.handdrop").html('');
        $.each(sc2handdropboxObj, function (key, value) {
            $(".btn.zoom.object.handdrop").append("<button class='btn s2 handdrop change object arrow" + key + "' data-cur-val='" + value + "'><img src='img/" + value + ".png' /></div>");
        });
    }
}

function zoomobjectsc2(thise, oj) {
    zoom_obj = oj;
    $('.screen-2').hide();
    $('.screen-2-zoom-' + oj).show();
    displaysc2ColorNoDigitboxAob();
    displaysc2DotboxObjAob();
    displaysc2handdropboxAob();
}
function displaysc2ColorNoDigitboxAob() {
    $(".screen-2-zoom-box_color_no .screen-object.box_color_no").html('');
    $.each(sc2ColorNoDigitboxObj, function (key, value) {
        $(".screen-2-zoom-box_color_no .screen-object.box_color_no").append("<button class='btn s2 box_color_no change object arrow" + key + "' data-cur-val='" + value + "' onclick=\"changeobjectsc2ColorNobox(this," + key + ")\"><img src='img/" + value + ".png' /></div>");
    });
}
function changeobjectsc2ColorNobox(thise, key) {
    var oj = $('.btn.s2.box_color_no.change.object.arrow' + key).attr("data-cur-val");

    var lastChar = parseInt(oj.slice(-1));
    if (lastChar == 10) {
        lastChar = 1;
    } else {
        lastChar = lastChar + 1;
    }
    sc2ColorNoDigitboxObj[key] = 'number/' + lastChar;
    $('.btn.s2.box_color_no.change.object.arrow' + key).attr("data-cur-val", sc2ColorNoDigitboxObj[key]);
    $('.btn.s2.box_color_no.change.object.arrow' + key).html("<img src='img/" + sc2ColorNoDigitboxObj[key] + ".png' />");

    var allTrue = true;
    $.each(sc2ColorNoDigitboxObj, function (key, value) {
        if (allTrue && sc2ColorNoDigitboxObj[key] != sc2ColorNoDigitboxObjResult[key]) {
            allTrue = false;
        }
    });
    if (allTrue) {
        $('.screen-2-zoom-box_color_no .screen-object.box_color_no-box0002').show();
        $('.screen-2-zoom-box_color_no .screen-object.box_color_no').hide();
        $('.screen-2 .btn.zoom.object.box_color_no').addClass('final').html("<img class='sc2ref hammer' src='img/hammer.png' />");
    }
}

function dropspot(ev) {
    ev.preventDefault();
    var obj1inv = ev.dataTransfer.getData("obj1inv");
    console.log(obj1inv);
    obj1inv='hammer';
    if ('hammer' == obj1inv) {
        invObj.splice(invObj.indexOf(obj1inv), 1);
        displayInvitems();
        $('.screen-2-zoom-bot .screen-object.bot-box0002').show();
        $('.screen-2-zoom-bot .screen-object.bot').hide();
        $('.screen-2 .btn.zoom.object.bot').addClass('final').removeAttr("onclick").html("<img src='img/pot_break.png' />");
    }
}
function sc2dropsbutton(ev) {
    ev.preventDefault();
    var obj1inv = ev.dataTransfer.getData("obj1inv");
    console.log(obj1inv);
    obj1inv='tv_button0002';
    if ('tv_button0002' == obj1inv) {
        invObj.splice(invObj.indexOf(obj1inv), 1);
        displayInvitems();
        $('.btn.zoom.object.tv_remotetext').show();
        $('.btn.zoom.object.tv_button0001').addClass('final').html("<img src='img/tv_button0002.png' />"); 
    }
}
function displaysc2DotboxObjAob() {
    $(".screen-2-zoom-f2BOX0001dot .screen-object.f2BOX0001dot").html('');
    $.each(sc2DotboxObj, function (key, value) {
        $(".screen-2-zoom-f2BOX0001dot .screen-object.f2BOX0001dot").append("<button class='btn s2 f2BOX0001dot change object arrow" + key + "' data-cur-val='" + value + "' onclick=\"changeobjectsc2DotboxObj(this," + key + ")\"><img src='img/" + value + ".png' /></div>");
    });
}
function changeobjectsc2DotboxObj(thise, key) {
    sc2DotboxObjPushResult.push(key);
    if(key==0) {
        sc2DotboxObj[0]='click_button0001';
        sc2DotboxObj[1]='LR_button';
    } else {
        sc2DotboxObj[1]='click_button0001';
        sc2DotboxObj[0]='LR_button';
    }
    displaysc2DotboxObjAob();
    if (sc2DotboxObjPushResult.toString()==sc2DotboxObjResult.toString()) {
        invObj.splice(invObj.indexOf('scroll_scroll2'), 1);
        displayInvitems();
        $('.screen-2-zoom-f2BOX0001dot .screen-object.f2BOX0001dot-box0002').show();
        $('.screen-2-zoom-f2BOX0001dot .screen-object.f2BOX0001dot').hide();
        $('.screen-2 .btn.zoom.object.f2BOX0001dot').addClass('final').html("<img  class='sc2ref color_clue' src='img/color_clue.png' />");
    }
}


function displaysc2handdropboxAob() {
    $(".screen-2-zoom-handdrop .screen-object.handdrop").html('');
    $.each(sc2handdropboxObj, function (key, value) {
        if ('hand_0002' == value) {
            $(".screen-object.handdrop").append("<div class='btn s2 change object handdrop btn" + key + "'><img src='img/" + value + ".png' /></div>");
        } else {
            $(".screen-object.handdrop").append("<div ondrop=\"drops2handdropbox(event)\" ondragover=\"allowDrop(event)\" class='btn s2 change object handdrop btn" + key + "' data-cur-val='" + value + "'><img src='img/" + value + ".png' /></div>");
        }
    });
}

function drops2handdropbox(ev) {
    ev.preventDefault();
    var obj1inv = ev.dataTransfer.getData("obj1inv");
    if ('hand_0002' == obj1inv) {
        sc2handdropboxObj[sc2handdropboxO.length] = obj1inv;
        invObj.splice(invObj.indexOf(obj1inv), 1);
        sc2handdropboxO.push(obj1inv);
        displaysc2handdropboxAob();
        displayInvitems();

        if (sc2handdropboxO.length === 4) {
            $('.screen-object.handdrop-box0002').show();
            $('.screen-object.handdrop').hide();
            $('.btn.zoom.object.handdrop').addClass('final').html("<img class='sc2ref final_key0001' src='img/final_key0001.png' />");
        }
    }

}