var sc1boxRemoteObj = { 0: 'alpha/A-Z_0001', 1: 'alpha/A-Z_0001', 2: 'alpha/A-Z_0001', 3: 'alpha/A-Z_0001', 4: 'alpha/A-Z_0001', 5: 'alpha/A-Z_0001' };
// var sc1boxRemoteResult = { 0: 'alpha/A-Z_0002', 1: 'alpha/A-Z_0001', 2: 'alpha/A-Z_0001', 3: 'alpha/A-Z_0001', 4: 'alpha/A-Z_0001', 5: 'alpha/A-Z_0001' };
var sc1boxRemoteResult = { 0: 'alpha/A-Z_0009', 1: 'alpha/A-Z_0022', 2: 'alpha/A-Z_0014', 3: 'alpha/A-Z_0013', 4: 'alpha/A-Z_0006', 5: 'alpha/A-Z_0022' };

var sc1horsecollector = [];
var sc1horsecollectorbuttonObj = { 0: 'hou-obj', 1: 'hou-obj', 2: 'hou-obj', 3: 'hou-obj', 4: 'hou-obj' };
// var sc1horsecollectorbuttonObjResulT = { 0: 'hou-obj1', 1: 'hou-obj', 2: 'hou-obj', 3: 'hou-obj', 4: 'hou-obj' };
var sc1horsecollectorbuttonObjResulT = { 0: 'hou-obj1', 1: 'hou-obj1', 2: 'hou-obj1', 3: 'hou-obj1', 4: 'hou-obj1' };


var sc1objectdiamond9buttonObj = { 0: 'UD0001', 1: 'UD0001', 2: 'UD0001', 3: 'UD0001', 4: 'UD0001', 5: 'UD0001'};
// var sc1objectdiamond9buttonObjResulT = { 0: 'UD0002', 1: 'UD0001', 2: 'UD0001', 3: 'UD0001', 4: 'UD0001', 5: 'UD0001'};
var sc1objectdiamond9buttonObjResulT = { 0: 'UD0003', 1: 'UD0002', 2: 'UD0002', 3: 'UD0003', 4: 'UD0002', 5: 'UD0003'};


var sc1lrboxbuttonObj = { 0: 'lrcolor0001', 1: 'lrcolor0002', 2: 'lrcolor0003', 3: 'lrcolor0004', 4: 'lrcolor0005', 5: 'lrcolor0006' };
// var sc1lrboxbuttonObjResulT = { 0: 'lrcolor0001', 1: 'lrcolor0002', 2: 'lrcolor0003', 3: 'lrcolor0004', 4: 'lrcolor0005', 5: 'lrcolor0006' };
var sc1lrboxbuttonObjResulT = { 0: 'lrcolor0006', 1: 'lrcolor0005', 2: 'lrcolor0001', 3: 'lrcolor0004', 4: 'lrcolor0002', 5: 'lrcolor0003' };

function defaultDisplaySc1() {
    if(!$(".btn.zoom.object.box_remote").hasClass('final')){
        $(".btn.zoom.object.box_remote").html('');
        $.each(sc1boxRemoteObj, function (key, value) {
            $(".btn.zoom.object.box_remote").append("<button class='btn s1 box_remote change object arrow" + key + "' data-cur-val='" + value + "'><img src='img/" + value + ".png' /></div>");
        });
    }
    if(!$(".btn.zoom.object.horsecollector").hasClass('final')){
        $(".btn.zoom.object.horsecollector").html('');
        $.each(sc1horsecollectorbuttonObj, function (key, value) {
            $(".btn.zoom.object.horsecollector").append("<button class='btn s1 horsecollector change object arrow" + key + "' data-cur-val='" + value + "'><img src='img/" + value + ".png' /></div>");
        });
    }
    if(!$(".btn.zoom.object.objectdiamond9").hasClass('final')){
        $(".btn.zoom.object.objectdiamond9").html('');
        $.each(sc1objectdiamond9buttonObj, function (key, value) {
            $(".btn.zoom.object.objectdiamond9").append("<button class='btn s1 objectdiamond9 change object arrow" + key + "' data-cur-val='" + value + "'><img src='img/" + value + ".png' /></div>");
        });
    }
    if(!$(".btn.zoom.object.lrbox").hasClass('final')){
        $(".btn.zoom.object.lrbox").html('');
        $.each(sc1lrboxbuttonObj, function (key, value) {
            $(".btn.zoom.object.lrbox").append("<button class='btn s1 lrbox change object arrow" + key + "' data-cur-val='" + value + "'><img src='img/" + value + ".png' /></div>");
        });
    }
}
function zoomobjectsc1(thise, oj) {
    zoom_obj = oj;
    $('.screen-1').hide();
    $('.screen-1-zoom-' + oj).show();
    displaysc1boxRemoteboxAob();
    displaysc1horsecollectorboxAob();
    displaysc1diamond9ObjboxAob();
    displaysc1lrboxButtonboxAob();
}
function displaysc1boxRemoteboxAob() {
    $(".screen-1-zoom-box_remote .screen-object.box_remote").html('');
    $.each(sc1boxRemoteObj, function (key, value) {
        $(".screen-1-zoom-box_remote .screen-object.box_remote").append("<button class='btn s1 box_remote change object arrow" + key + "' data-cur-val='" + value + "' onclick=\"changeobjectsc1boxRemotebox(this," + key + ")\"><img src='img/" + value + ".png' /></div>");
    });
}
function changeobjectsc1boxRemotebox(thise, key) {
    var oj = $('.btn.s1.box_remote.change.object.arrow' + key).attr("data-cur-val");

    var lastChar = parseInt(oj.slice(-2));
    if (lastChar == 26) {
        lastChar = 1;
    } else {
        lastChar = lastChar + 1;
    }
    sc1boxRemoteObj[key] = (lastChar>9)?('alpha/A-Z_00' + lastChar):('alpha/A-Z_000' + lastChar);
    $('.btn.s1.box_remote.change.object.arrow' + key).attr("data-cur-val", sc1boxRemoteObj[key]);
    $('.btn.s1.box_remote.change.object.arrow' + key).html("<img src='img/" + sc1boxRemoteObj[key] + ".png' />");

    var allTrue = JSON.stringify(sc1boxRemoteObj)==JSON.stringify(sc1boxRemoteResult);
    if (allTrue) {
        $('.screen-1-zoom-box_remote .screen-object.box_remote-box0002').show();
        $('.screen-1-zoom-box_remote .screen-object.box_remote').hide();
        $('.screen-1 .btn.zoom.object.box_remote').addClass('final').html("<img class='sc1ref ball-obj' src='img/ball-obj.png' /><img class='sc1ref ball-obj' src='img/ball-obj.png' />");
    }
}
function displaysc1horsecollectorboxAob() {
    $(".screen-1-zoom-horsecollector .screen-object.horsecollector").html('');
    $.each(sc1horsecollectorbuttonObj, function (key, value) {
        if ('hou-obj1' == value) {
            $(".screen-object.horsecollector").append("<div class='btn s1 horsecollector change object btn" + key + "'><img src='img/" + value + ".png' /></div>");
        } else {
            $(".screen-object.horsecollector").append("<div ondrop=\"drops1horsecollector(event)\" ondragover=\"allowDrop(event)\" class='btn s1 horsecollector change object btn" + key + "' data-cur-val='" + value + "'><img src='img/" + value + ".png' /></div>");
        }
    });
}
function drops1horsecollector(ev) {
    ev.preventDefault();
    var obj1inv = ev.dataTransfer.getData("obj1inv");
    if ('hou-obj1' == obj1inv) {
        sc1horsecollectorbuttonObj[sc1horsecollector.length] = obj1inv;
        invObj.splice(invObj.indexOf(obj1inv), 1);
        sc1horsecollector.push(obj1inv);
        displaysc1horsecollectorboxAob();
        displayInvitems();

        if (sc1horsecollector.length === 5) {
            $('.screen-object.horsecollector-box0002').show();
            $('.screen-object.horsecollector').hide();
            $('.btn.zoom.object.horsecollector').addClass('final').html("<img class='sc1ref hight_clue' src='img/knife.png' />");
        }
    }

}

function displaysc1diamond9ObjboxAob() {
    $(".screen-1-zoom-objectdiamond9 .screen-object.objectdiamond9").html('');
    $.each(sc1objectdiamond9buttonObj, function (key, value) {
        $(".screen-1-zoom-objectdiamond9 .screen-object.objectdiamond9").append("<button class='btn s1 objectdiamond9 change object arrow" + key + "' data-cur-val='" + value + "' onclick=\"changeobjectsc1diamond9Objbox(this," + key + ")\"><img src='img/" + value + ".png' /></div>");
    });
}
function changeobjectsc1diamond9Objbox(thise, key) {
    var oj = $('.btn.s1.objectdiamond9.change.object.arrow' + key).attr("data-cur-val");

    var lastChar = parseInt(oj.slice(-1));
    if (lastChar == 3) {
        lastChar = 1;
    } else {
        lastChar = lastChar + 1;
    }
    sc1objectdiamond9buttonObj[key] = 'UD000' + lastChar;
    $('.btn.s1.objectdiamond9.change.object.arrow' + key).attr("data-cur-val", sc1objectdiamond9buttonObj[key]);
    $('.btn.s1.objectdiamond9.change.object.arrow' + key).html("<img src='img/" + sc1objectdiamond9buttonObj[key] + ".png' />");

    var allTrue = JSON.stringify(sc1objectdiamond9buttonObj)==JSON.stringify(sc1objectdiamond9buttonObjResulT);
    if (allTrue) {
        invObj.splice(invObj.indexOf('scroll_scroll3'), 1);
        displayInvitems();
        $('.screen-1-zoom-objectdiamond9 .screen-object.objectdiamond9-box0002').show();
        $('.screen-1-zoom-objectdiamond9 .screen-object.objectdiamond9').hide();
        $('.screen-1 .btn.zoom.object.objectdiamond9').addClass('final').html("<img class='sc1ref hight_clue' src='img/hight_clue.png' /><img class='sc1ref hand_0002' src='img/hand_0002.png' />");
    }
}



function displaysc1lrboxButtonboxAob() {
    $(".screen-1-zoom-lrbox .screen-object.lrbox").html('');
    $.each(sc1lrboxbuttonObj, function (key, value) {
        $(".screen-1-zoom-lrbox .screen-object.lrbox").append("<button class='btn s1 lrbox change object arrow" + key + "' data-cur-val='" + value + "' onclick=\"changeobjectsc1lrboxButtonObjbox(this," + key + ")\"><img src='img/" + value + ".png' /></div>");
    });
}
var parentSc1lrbox='';
var parentSc1lrboxIndex='';
function changeobjectsc1lrboxButtonObjbox(thise, key) {
    var oj = $(thise).attr("data-cur-val");
    if(parentSc1lrbox=='') {
        parentSc1lrbox=oj;
        parentSc1lrboxIndex=key;
    } else {
        sc1lrboxbuttonObj[parentSc1lrboxIndex]=oj;
        sc1lrboxbuttonObj[key]=parentSc1lrbox;
        parentSc1lrbox='';
    } 
    displaysc1lrboxButtonboxAob();
    var allTrue = true;
    $.each(sc1lrboxbuttonObj, function (key, value) {
        if (allTrue && sc1lrboxbuttonObj[key] != sc1lrboxbuttonObjResulT[key]) {
            allTrue = false;
        }
    });
    if (allTrue) {
        invObj.splice(invObj.indexOf('scroll_scroll4'), 1);
        displayInvitems();
        $('.screen-1-zoom-lrbox .screen-object.lrbox-box0002').show();
        $('.screen-1-zoom-lrbox .screen-object.lrbox').hide();
        $('.screen-1 .btn.zoom.object.lrbox').addClass('final').html("<img class='sc1ref hand' src='img/hand_0002.png' /><img class='sc1ref hand' src='img/hand_0002.png' />");
    }
        
}