var sc5symObj = { 0: 'sym0001', 1: 'sym0002', 2: 'sym0003', 3: 'sym0004' };
var sc5symObjResulT = { 0: 'sym0004', 1: 'sym0002', 2: 'sym0003', 3: 'sym0001' };

var sc5heightdotbuttonObj = { 0: 'hight_0001', 1: 'hight_0001', 2: 'hight_0001', 3: 'hight_0001' };
// var sc5heightdotbuttonObjResulT = { 0: 'hight_0002', 1: 'hight_0001', 2: 'hight_0001', 3: 'hight_0001' };
var sc5heightdotbuttonObjResulT = { 0: 'hight_0005', 1: 'hight_0003', 2: 'hight_0002', 3: 'hight_0004' };

function defaultDisplaySc5() {
    if(!$(".btn.zoom.object.smile").hasClass('final')){
        $(".btn.zoom.object.smile").html('');
        $.each(sc5symObj, function (key, value) {
            $(".btn.zoom.object.smile").append("<button class='btn s5 symobj change object arrow" + key + "' data-cur-val='" + value + "'><img src='img/" + value + ".png' /></div>");
        });
    }
    if(!$(".btn.zoom.object.heightdot").hasClass('final')){
        $(".btn.zoom.object.heightdot").html('');
        $.each(sc5heightdotbuttonObj, function (key, value) {
            $(".btn.zoom.object.heightdot").append("<button class='btn s5 heightdot change object arrow" + key + "' data-cur-val='" + value + "'><img src='img/" + value + ".png' /></div>");
        });
    }
}
function zoomobjectsc5(thise, oj) {
    zoom_obj = oj;
    $('.screen-5').hide();
    $('.screen-5-zoom-' + oj).show();
    displaysc5symobjObjboxAob();
    displaysc5heightdotObjboxAob();
}
function displaysc5symobjObjboxAob() {
    $(".screen-5-zoom-symobj .screen-object.symobj").html('');
    $.each(sc5symObj, function (key, value) {
        $(".screen-5-zoom-symobj .screen-object.symobj").append("<button class='btn s5 symobj change object arrow" + key + "' data-cur-val='" + value + "' onclick=\"changeobjectsc5symObjbox(this," + key + ")\"><img src='img/" + value + ".png' /></div>");
    });
}
function changeobjectsc5symObjbox(thise, key) {
    var oj = $('.btn.s5.symobj.change.object.arrow' + key).attr("data-cur-val");

    var lastChar = parseInt(oj.slice(-1));
    if (lastChar == 4) {
        lastChar = 1;
    } else {
        lastChar = lastChar + 1;
    }
    sc5symObj[key] = 'sym000' + lastChar;
    $('.btn.s5.symobj.change.object.arrow' + key).attr("data-cur-val", sc5symObj[key]);
    $('.btn.s5.symobj.change.object.arrow' + key).html("<img src='img/" + sc5symObj[key] + ".png' />");

    var allTrue = JSON.stringify(sc5symObj)==JSON.stringify(sc5symObjResulT);
    // allTrue=true;
    if (allTrue) {
        invObj.splice(invObj.indexOf('scroll_scroll1'), 1);
        displayInvitems();
        $('.screen-5-zoom-symobj .screen-object.symobj-box0002').show();
        $('.screen-5-zoom-symobj .screen-object.symobj').hide();
        $('.screen-5 .btn.zoom.object.smile').addClass('final').html("<img src='img/tv_button0002.png' />").css('width','100px');
    }
}
function drops5kinf(ev) {
    ev.preventDefault();
    var obj1inv = ev.dataTransfer.getData("obj1inv");
    if ('knife' == obj1inv) {
        invObj.splice(invObj.indexOf(obj1inv), 1);
        displayInvitems();
        $('.btn.zoom.object.s5_knif').addClass('final').html("<img src='img/knife_work0002.png' />");
    }

}
function displaysc5heightdotObjboxAob() {
    $(".screen-5-zoom-heightdot .screen-object.heightdot").html('');
    $.each(sc5heightdotbuttonObj, function (key, value) {
        $(".screen-5-zoom-heightdot .screen-object.heightdot").append("<button class='btn s5 heightdot change object arrow" + key + "' data-cur-val='" + value + "' onclick=\"changeobjectsc5heightdotObjbox(this," + key + ")\"><img src='img/" + value + ".png' /></div>");
    });
}
function changeobjectsc5heightdotObjbox(thise, key) {
    var oj = $('.btn.s5.heightdot.change.object.arrow' + key).attr("data-cur-val");

    var lastChar = parseInt(oj.slice(-1));
    if (lastChar == 5) {
        lastChar = 1;
    } else {
        lastChar = lastChar + 1;
    }
    sc5heightdotbuttonObj[key] = 'hight_000' + lastChar;
    $('.btn.s5.heightdot.change.object.arrow' + key).attr("data-cur-val", sc5heightdotbuttonObj[key]);
    $('.btn.s5.heightdot.change.object.arrow' + key).html("<img src='img/" + sc5heightdotbuttonObj[key] + ".png' />");

    var allTrue = JSON.stringify(sc5heightdotbuttonObj)==JSON.stringify(sc5heightdotbuttonObjResulT);
    // allTrue=true;
    if (allTrue) {
        invObj.splice(invObj.indexOf('hight_clue'), 1);
        displayInvitems();
        $('.screen-5-zoom-heightdot .screen-object.heightdot-box0002').show();
        $('.screen-5-zoom-heightdot .screen-object.heightdot').hide();
        $('.screen-5 .btn.zoom.object.heightdot').addClass('final').html("<img class='sc5ref heightdot' src='img/scroll_scroll4.png' /><img  class='sc5ref hand_0002' src='img/hand_0002.png' />");
    }
}

function drops5door(ev) {
    ev.preventDefault();
    var obj1inv = ev.dataTransfer.getData("obj1inv");
    if ('final_key0001' == obj1inv) {
        invObj.splice(invObj.indexOf(obj1inv), 1);
        displayInvitems();
        $('.btn.zoom.object.s5_door').addClass('final').html("");
        $('.btn.zoom.object.lasttext').show();
    }
}